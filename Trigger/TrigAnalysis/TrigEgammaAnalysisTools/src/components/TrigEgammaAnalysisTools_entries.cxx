#include "TrigEgammaAnalysisTools/TrigEgammaAnalysisBaseTool.h"
#include "../TrigEgammaMonTool.h"
#include "../TrigEgammaPhysValMonTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavBaseTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavAnalysisTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavTPBaseTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavTPAnalysisTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavNtuple.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavTPNtuple.h"
#include "TrigEgammaAnalysisTools/EfficiencyTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaResolutionTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaDistTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaPlotTool.h"
#include "../TrigEgammaAnalysisAlg.h"
#include "../TrigEgammaTDToolTest.h"

DECLARE_COMPONENT( TrigEgammaAnalysisBaseTool )
DECLARE_COMPONENT( TrigEgammaMonTool )
DECLARE_COMPONENT( TrigEgammaPhysValMonTool )
DECLARE_COMPONENT( TrigEgammaNavTPBaseTool )
DECLARE_COMPONENT( TrigEgammaNavTPNtuple )
DECLARE_COMPONENT( TrigEgammaNavAnalysisTool )
DECLARE_COMPONENT( TrigEgammaNavTPAnalysisTool )
DECLARE_COMPONENT( TrigEgammaNavNtuple )
DECLARE_COMPONENT( EfficiencyTool ) 
DECLARE_COMPONENT( TrigEgammaResolutionTool ) 
DECLARE_COMPONENT( TrigEgammaDistTool ) 
DECLARE_COMPONENT( TrigEgammaPlotTool ) 
DECLARE_COMPONENT( TrigEgammaAnalysisAlg )
DECLARE_COMPONENT( TrigEgammaTDToolTest )
    

